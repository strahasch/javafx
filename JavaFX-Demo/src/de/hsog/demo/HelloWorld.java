package de.hsog.demo;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class HelloWorld extends Application {
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		
		// zuerst die einzelnen UI-Controls erstellen
		Button btn = new Button("Hello");
		btn.setText("Say 'Hello World'");
		
		// hier wird eine anonyme innere Klasse mit 
		// new EventHandler ... erzeugt
		btn.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				System.out.println("Hello Offenburg!");
			}
		});

		// die UI-Controls werden einem 
		// Layout-Manager zugeordnet.
		// In Java hei�t ein Layout-Manager Pane
		StackPane root = new StackPane();
		root.getChildren().add(btn);
		
		// dann wird der Pane der Scene zugeordnet
		Scene helloWorldScene = new Scene(root, 600, 250);
		
		// und zuguter letzt wird die Scene der Stage zugeordnet.
		primaryStage.setScene(helloWorldScene);
		primaryStage.setTitle("Hello World!");
		
		// und dann schlie�lich dargestellt
		primaryStage.show();
		
	}
}
