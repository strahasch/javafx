package de.hsog.demo.panes;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class BorderPaneExample extends Application {
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		primaryStage.setTitle("BorderPane");

		// Inhalt des Fensters anordnen
		BorderPane borderPane = new BorderPane();

		// Widgets zur BorderPane hinzufügen
		Label topLabel = new Label("Titel");
		BorderPane.setAlignment(topLabel, Pos.CENTER_LEFT);
		BorderPane.setMargin(topLabel, new Insets(4, 4, 4, 4));
		borderPane.setTop(topLabel);

		borderPane.setLeft(new Button("Left"));
		borderPane.setRight(new Button("Right"));

		TextArea centerText = new TextArea("Center");
		BorderPane.setAlignment(centerText, Pos.CENTER);
		BorderPane.setMargin(centerText, new Insets(4, 4, 4, 4));
		borderPane.setCenter(centerText);

		borderPane.setBottom(new Button("Bottom"));

		primaryStage.setScene(new Scene(borderPane, 300, 250));
		primaryStage.show();
	}
}
