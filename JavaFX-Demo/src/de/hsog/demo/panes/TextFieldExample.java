package de.hsog.demo.panes;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class TextFieldExample extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		primaryStage.setTitle("Example of a text field.");

		// Creating a GridPane container
		GridPane grid = new GridPane();
		grid.setPadding(new Insets(10, 10, 10, 10));
		grid.setVgap(5);
		grid.setHgap(5);

		// Defining the Name text field
		Label label1 = new Label("First name:");
		final TextField name = new TextField();
		name.setPromptText("Enter your first name.");
		HBox hb1 = new HBox();
		hb1.getChildren().addAll(label1, name);
		hb1.setSpacing(10);
		GridPane.setConstraints(hb1, 0, 0);
		grid.getChildren().add(hb1);

		// Defining the Last Name text field
		Label label2 = new Label("Last name:");
		final TextField lastName = new TextField();
		lastName.setPromptText("Enter your last name.");
		HBox hb2 = new HBox();
		hb2.getChildren().addAll(label2, lastName);
		hb2.setSpacing(10);
		GridPane.setConstraints(hb2, 0, 1);
		grid.getChildren().add(hb2);

		// Defining the Comment text field
		Label label3 = new Label("Comment:");
		final TextField comment = new TextField();
		comment.setPromptText("Enter your comment.");
		HBox hb3 = new HBox();
		hb3.getChildren().addAll(label3, comment);
		hb3.setSpacing(10);
		GridPane.setConstraints(hb3, 0, 2);
		grid.getChildren().add(hb3);

		// Defining the Submit button
		Button submit = new Button("Submit");
		GridPane.setConstraints(submit, 1, 0);
		grid.getChildren().add(submit);

		// Defining the Clear button
		Button clear = new Button("Clear");
		GridPane.setConstraints(clear, 1, 1);
		grid.getChildren().add(clear);

		// Show Gird
		primaryStage.setScene(new Scene(grid, 400, 200));
		primaryStage.show();
	}
}
