package de.hsog.demo.panes;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class BorderPaneHBox extends Application {

	@Override
	public void start(Stage primaryStage) {
		BorderPane borderPane = new BorderPane();
		
		borderPane.setTop(new Label("Titel"));
			
		Image image = new Image(getClass().getResourceAsStream("/resource/turbine.jpg"));
		Label leftLabel = new Label("", new ImageView(image));
		BorderPane.setAlignment(leftLabel, Pos.CENTER);
		borderPane.setLeft(leftLabel);

		TextArea centerText = new TextArea("Center");
		borderPane.setCenter(centerText);

		HBox buttonBox = new HBox(4.0);
		buttonBox.setAlignment(Pos.BOTTOM_RIGHT);
		buttonBox.getChildren().add(new Button("Weiter"));
		buttonBox.getChildren().add(new Button("Abbruch"));
		BorderPane.setMargin(buttonBox, new Insets(6, 0, 4, 0));

		borderPane.setBottom(buttonBox);
		primaryStage.setScene(new Scene(borderPane, 300, 250));
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
