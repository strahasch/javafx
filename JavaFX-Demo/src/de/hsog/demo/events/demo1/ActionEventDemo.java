package de.hsog.demo.events.demo1;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class ActionEventDemo extends Application {
	
	final TextArea centerText = new TextArea("");

	@Override
	public void start(Stage primaryStage) {
		primaryStage.setTitle("ActionEvent-Demo");

		// Inhalt des Fensters mit einem LayoutManager (Pane) anordnen
		BorderPane borderPane = new BorderPane();

		// Controls zum BorderLayout hinzufuegen
		borderPane.setTop(new Label("ActionEvent-Demo"));

		// Image einbinden
		Image image = new Image(getClass().getResourceAsStream(
				"/resource/java.jpg"));
		Label leftLabel = new Label("", new ImageView(image));
		BorderPane.setAlignment(leftLabel, Pos.CENTER);
		borderPane.setLeft(leftLabel);

		// Textbereich
		borderPane.setCenter(centerText);

		// Buttons hinzuf�gen
		HBox buttonBox = new HBox(10);
		buttonBox.setAlignment(Pos.BOTTOM_RIGHT);
		Button sayButton = new Button("Say 'Hello'");
		Button goodbyeButton = new Button("Goodbye");
		Button cancelButton = new Button("Abbruch");
		buttonBox.getChildren().add(sayButton);
		buttonBox.getChildren().add(goodbyeButton);
		buttonBox.getChildren().add(cancelButton);
		BorderPane.setMargin(buttonBox, new Insets(4.0, 4.0, 4.0, 4.0));

		borderPane.setBottom(buttonBox);

		sayButton.setOnAction(new EventHandler<ActionEvent>() {
			// dies ist eine sogenannte innere anoyme Klasse
			// dies ist nur ratsam f�r einfaches Eventhandling
			@Override
			public void handle(ActionEvent e) {
				centerText.appendText("Hello!\n");
			}
		});
		
		//�bergabe von this (=dieses Objekt), damit der Controller
		//auf die Attribute dieser Klasse zugreifen kann.
		//Controller ben�tigt Referenz auf diese Klasse.
		goodbyeButton.setOnAction(new GoodbyeController(this));
		
		borderPane.setOnKeyPressed(new PaneKeyController(this));

		// Kurzform mit Lambdas
		// sayButton.setOnAction(event -> centerText.appendText("Hello!\n"));

		Scene scene = new Scene(borderPane);

		primaryStage.setScene(scene);
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}

}
