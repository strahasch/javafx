package de.hsog.demo.events.demo1;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class GoodbyeController implements EventHandler<ActionEvent> {

	// notwendig, um auf den Textbereich zugreifen zu k�nnen
	ActionEventDemo main;
	
	public GoodbyeController(ActionEventDemo m){
		this.main = m;
	}
	
	public void handle(ActionEvent event) {
		//AUsgabe auf der Konsole
		System.out.println("Goodbye...");
		
		//Textinhalt �ndern
		main.centerText.appendText("Goodbye!\n");
		
		System.out.println(event);
	}
}
