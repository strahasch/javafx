package de.hsog.demo.events.demo1;

import javafx.event.EventHandler;
import javafx.scene.input.KeyEvent;

public class PaneKeyController implements EventHandler<KeyEvent> {

	// notwendig, um auf den Textbereich zugreifen zu k�nnen
	ActionEventDemo main;

	public PaneKeyController(ActionEventDemo m) {
		this.main = m;
	}
	
	public void handle(KeyEvent keyEvent) {

		// Textinhalt �ndern
		main.centerText.appendText("You pressed key "+keyEvent.getCode()+".\n");
		
		// Ausgabe auf der Konsole
		System.out.println("Key "+keyEvent.getCode()+" was pressed.");
	}
}
